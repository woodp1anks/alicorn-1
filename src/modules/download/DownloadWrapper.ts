import { DownloadMeta, DownloadStatus } from "./AbstractDownloader";
import { Concurrent } from "./Concurrent";
import { Serial } from "./Serial";
import { applyMirror } from "./Mirror";
import { getNumber } from "../config/ConfigSupport";
import EventEmitter from "events";
import { getModifiedDate, isFileExist } from "../config/FileUtil";
import { validate } from "./Validate";
import {
  deleteRecord,
  getLastValidateModified,
  updateRecord,
} from "../container/ValidateRecord";

const PENDING_TASKS: DownloadMeta[] = [];
const RUNNING_TASKS = new Set<DownloadMeta>();
const WAITING_RESOLVES_MAP = new Map<
  DownloadMeta,
  (value: DownloadStatus | PromiseLike<DownloadStatus>) => void
>();
const END_GATE = "END";
let MAX_TASKS: number;
let EMITTER: EventEmitter;

export function initDownloadWrapper(): void {
  MAX_TASKS = getNumber("download.concurrent.max-tasks", 20);
  EMITTER = new EventEmitter();
  EMITTER.on(END_GATE, (m: DownloadMeta, s: DownloadStatus) => {
    RUNNING_TASKS.delete(m);
    (
      WAITING_RESOLVES_MAP.get(m) ||
      (() => {
        return;
      })
    )(s);
    scheduleNextTask();
  });
}

// Download one file
// Mirror will be applied here
// There are no options for user to choose downloader
// Concurrent will always be used first
// If file already exists, downloader will resolve if hash matches
export async function wrappedDownloadFile(
  meta: DownloadMeta
): Promise<DownloadStatus> {
  const mirroredMeta = new DownloadMeta(
    applyMirror(meta.url),
    meta.savePath,
    meta.sha1
  );
  if ((await _wrappedDownloadFile(mirroredMeta)) === DownloadStatus.RESOLVED) {
    return DownloadStatus.RESOLVED;
  }
  return await _wrappedDownloadFile(meta);
}

function _wrappedDownloadFile(meta: DownloadMeta): Promise<DownloadStatus> {
  return new Promise<DownloadStatus>((resolve) => {
    existsAndValidate(meta).then((b) => {
      if (b) {
        resolve(DownloadStatus.RESOLVED);
      } else {
        WAITING_RESOLVES_MAP.set(meta, resolve);
        PENDING_TASKS.push(meta);
        scheduleNextTask();
      }
    });
  });
}

function scheduleNextTask(): void {
  if (RUNNING_TASKS.size < MAX_TASKS && PENDING_TASKS.length > 0) {
    const tsk = PENDING_TASKS.pop();
    if (tsk !== undefined) {
      RUNNING_TASKS.add(tsk);
      downloadSingleFile(tsk, EMITTER);
    }
  }
}

function downloadSingleFile(meta: DownloadMeta, emitter: EventEmitter): void {
  Concurrent.getInstance()
    .downloadFile(meta)
    .then((s) => {
      if (s === DownloadStatus.RESOLVED) {
        emitter.emit(END_GATE, meta, DownloadStatus.RESOLVED);
      } else {
        Serial.getInstance()
          .downloadFile(meta)
          .then((s) => {
            emitter.emit(END_GATE, meta, s);
          });
      }
    });
}

// If no sha provided, we'll ignore it
async function existsAndValidate(meta: DownloadMeta): Promise<boolean> {
  if (!(await isFileExist(meta.savePath))) {
    deleteRecord(meta.savePath);
    return false;
  }
  if (meta.sha1.trim() === "") {
    // This might be a wrong SHA, we should not cache it
    return true;
  }
  const lastValidated = await getLastValidateModified(meta.savePath);
  const actualModifiedDate = await getModifiedDate(meta.savePath);
  if (actualModifiedDate <= lastValidated) {
    return true;
  }
  const res = await validate(meta.savePath, meta.sha1);
  if (res) {
    updateRecord(meta.savePath);
  } else {
    deleteRecord(meta.savePath);
  }
  return res;
}
